package com.example.locationexample;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.support.v4.app.NavUtils;

public class MainActivity extends Activity {
	private LocationListener listener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        this.listener = new LocationListener() {
			@Override
			public void onLocationChanged(Location loc) {
				// TODO Auto-generated method stub
				TextView tv = (TextView) findViewById(
						R.id.display);
				tv.setText(String.format(
						"Latitude %f, Longitude %f",
						loc.getLatitude(),
						loc.getLongitude()));
			}

			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				// TODO Auto-generated method stub
				
			}
        	
        };

        Button listen = (Button) findViewById(R.id.listen);
        listen.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				LocationManager locationManager =
						(LocationManager) getSystemService(
								Context.LOCATION_SERVICE);
				// NOTE: You can ONLY test with a GPS_PROVIDER!
				locationManager.requestLocationUpdates(
						LocationManager.GPS_PROVIDER, 0, 0,
						MainActivity.this.listener);
				Location loc = locationManager.getLastKnownLocation(
						LocationManager.GPS_PROVIDER);
				TextView tv = (TextView) findViewById(
						R.id.display);
				// NOTE: getLastKnownLocation() may return null!
				if (loc != null) {
					tv.setText(String.format(
							"Latitude %f, Longitude %f",
							loc.getLatitude(),
							loc.getLongitude()));
				}
			}
        	
        });
        
        Button stopListening = (Button) findViewById(
        		R.id.stop_listening);
        stopListening.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				LocationManager locationManager =
						(LocationManager) getSystemService(
								Context.LOCATION_SERVICE);
				locationManager.removeUpdates(listener);
				TextView tv = (TextView) findViewById(
						R.id.display);
				tv.setText("Not listening.");
			}
        	// 19.131637�N 72.915713�E
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    
}
